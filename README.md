# soal-shift-sisop-modul-2-ITA06-2022
Kelompok ITA06

1. Alda Risma Harjian (5027201004)
2. Richard Nicolas (5027201021)
3. Dzaki Indra Cahya (5027201053)

## Soal 1 (revisi)
jadi di disini 	Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.<br>
a.	Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).<br>
b.	Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA<br>
c.	Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.<br>
d.	Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880<br>
e.	Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)<br>

berikut Penyelesaian soal no.1, yang pertama diawali dengan menggunakan libraary:
```
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stddef.h>
#include <dirent.h>
```

Kita diminta mengunduh database item characters dan weapons, lalu mengekstrak kedua file tersebut. Kemudian praktikan membuat folder `gacha_gacha `untuk menampung hasil gacha.

```
char *dbase[] = {
        "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};
char *file[] = {"chara.zip", "weapon.zip"};
char *dirgacha[] = {"gacha_gacha"};

dbdownload(dbase[0], file[0]);
dbdownload(dbase[1], file[1]);
unzip(file[0]);
unzip(file[1]);
```
```
void dbdownload(char *dbase, char *file)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "-q", dbase, "-O", file, NULL};
        execv("/bin/wget", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
```
void unzip(char *file)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"unzip", "-q", "-n", file, NULL};
        execv("/bin/unzip", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
<ul>
<li> Kita mendefinisikan terlebih dahulu sumber unduhan database, serta nama file untuk menampungnya</li>
<li>Fungsi `dbdownload` menerima argumen tautan database serta nama file yang akan jadi output, kemudian mengunduh database dengan metode `wget`. Adapun flag `--no-check-certificate` diperlukan agar dapat mengunduh dari Google Drive, serta flag -q agar proses berjalan di background.</li>
<li>lalu fungsi `unzip` menerima file yang akan di unzip
</ul>

```
mkprocess(dirgacha[0]);
```

```
void mkprocess(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        DIR *checkDir = opendir(dir);
        if (checkDir)
        {
            closedir(checkDir); 
        }
        else
        {
            char *argv[] = {"mkdir", dir, NULL}; 
            execv("/bin/mkdir", argv);
        }
    }
    else if (child_id > 0)
    {
```

<ul>
<li> fungsi `mkprocess` untuk membuat direktori yang diterima lewat argumen `dir`</li>
</ul>
fungsi dibwah ini menjadi bagian dariberjalan nya sistem gacha:

```
void reduceprim(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 100)
    {
        zip();
        rmprocess("weapons");
        rmprocess("characters");
        stop();
    }
}
```
```
void reduceprim(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 100)
    {
        zip();
        rmprocess("weapons");
        rmprocess("characters");
        stop();
    }
}
```

<ul>
<li> mengurangi primogearl sebesar 160 tiap dilakukan proses gacha, dimana jumlah primogem ditentukan sebelumnya sebesar 79000. Adapun jika primogem telah di bawah jumlah yang diperlukan untuk gacha, maka memanggil fungsi `zip` dan `rmprocess`</li>
</ul>

```
void zip()
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        chdir("/home/rasy/sisop/soal-shift-sisop-modul-2-ita03-2022/soal1/gacha_gacha/");
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", ".", "-i", "*", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```

Mengzip direktori `gacha_gacha` dengan keluaran sesuai ketentuan soal, yakni nama output `not_safe_for_wibu.zip` dengan password `satuduatiga`

```
void rmprocess(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"rm", "-rf", dir, NULL};
        execv("/bin/rm", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```

<ul>
<li>Menghapus direktori yang telah ditentukan oleh `dir`</li>
</ul>
kita diminta membuat sistem gacha, dimana pada tiap kali jumlah gacha bernilai genap maka dilakukan gacha weapon, dan bila ganjil maka gacha character. Pada tiap jumlah gacha kelipatan 10 atau tenpull, output dimuat dalam file text baru. Pada tiap jumlah gacha kelipatan 90 atau hard pity, dibuat folder baru yang menampung hasil pencatatan 90 kali gacha sebelumnya.


```
void gachaproc(int nth, char gen_file[100])
{
    int len;
    char basepath[100], type[100];

    if (nth % 2 != 0)
    {
        len = 48;
        strcpy(basepath, "characters/");
        strcpy(type, "Characters");
    }
    if (nth % 2 == 0)
    {
        len = 130;
        strcpy(basepath, "weapons/");
        strcpy(type, "Weapons");
    }

    struct dirent *dp;
    DIR *dir = opendir(basepath);

    char temppath[50];
    int rn_gacha = rannum(len);
    if (dir != NULL)
    {
        for (int k = 0; k <= rn_gacha; k++)
        {
            dp = readdir(dir);
        }
        strcpy(temppath, dp->d_name);
        dp = NULL;
        closedir(dir);
    }
    else
    {
        perror("Couldn't open the directory");
    }

    strcat(basepath, temppath);
    FILE *fp;
    char temp[4096];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    fp = fopen(basepath, "r");
    fread(temp, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(temp);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    json_object_object_get_ex(parsed_json, "name", &name);

    FILE *output;
    output = fopen(gen_file, "a");

    char n[5], rest_prim[10];
    sprintf(rest_prim, "%d", primonum);
    sprintf(n, "%d", nth);

    char fileContext[100];
```


<ul>
<li>Dan Bila gacha berada pada kali yang ganjil, sistem akan menuju database character dan menetapkan `len` sebesar 48 yakni jumlah karakter. Jika genap, maka menuju database weapon dan `len` sebesar 130 dan ini code nya </li>
</ul>


```
if (nth % 2 != 0)
{
  len = 48;
  strcpy(basepath, "characters/");
  strcpy(type, "Characters");
}
if (nth % 2 == 0)
{
  len = 130;
  strcpy(basepath, "weapons/");
  strcpy(type, "Weapons");
}

struct dirent *dp;
DIR *dir = opendir(basepath);
```


<ul>
<li>Lalu setelah itu Melakukan directory listing pada database yang ditentukan, baik character maupun weapon. `rn_gacha` sebagai bilangan acak yang digenerate menentukan item yang diperoleh. Nama dari item akan dicopy pada `temppath` berikut code nya </li>
</ul>


```
char temppath[50];
int rn_gacha = rannum(len);
if (dir != NULL)
{
    for (int k = 0; k <= rn_gacha; k++)
    {
        dp = readdir(dir);
    }
    strcpy(temppath, dp->d_name);
    dp = NULL;
    closedir(dir);
}
else
{
    perror("Couldn't open the directory");
}
```


<ul>
<li> setelah itu Membaca dan memparsing item menggunakan library json-c/json.h, hingga didapat nama dan rarity item</li>
<li> danMendapat sisa primogem serta kali gacha keberapa dan ini sorce code nya </li>


```
strcat(basepath, temppath);
  FILE *fp;
  char temp[4096];
  struct json_object *parsed_json;
  struct json_object *name;
  struct json_object *rarity;

  fp = fopen(basepath, "r");
  fread(temp, 4096, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(temp);
  json_object_object_get_ex(parsed_json, "rarity", &rarity);
  json_object_object_get_ex(parsed_json, "name", &name);

  FILE *output;
  output = fopen(gen_file, "a");

  char n[5], rest_prim[10];
  sprintf(rest_prim, "%d", primonum);
  sprintf(n, "%d", nth);
```


<ul>
<li> lalu yang terakbirProgram membaca timestamp untuk memformat nama file gacha</li>
<li>Tiap iterasi dimana gacha dilakukan, gachacount bertambah, serta fungsi reduceprim dipanggil agar jumlah total primogem berkurang</li>
<li>rogram melakukan sleep(1) agar tiap file dibuat per satu detik. dan berikut source codenya </li>
</ul>


```
for (int i = 0; i < 9; i++)
{
  char gen_file[100], round[5];
  struct tm *ptr;
  time_t rawTime;
  rawTime = time(NULL);
  ptr = localtime(&rawTime);
  char temp[100];
  strftime(temp, 100, "%H:%M:%S", ptr);

  int checkRound = gachacount - (gachacount % 10);

  strcpy(gen_file, path);
  strcat(gen_file, "/");
  strcat(gen_file, temp);
  strcat(gen_file, "_gacha_");
  sprintf(round, "%d", checkRound);
  strcat(gen_file, round);
  strcat(gen_file, ".txt");

  for (size_t j = 0; j < 10; j++)
  {
    gachacount++;
    reduceprim(&primonum);
    gachaproc(gachacount, gen_file);
  }
  sleep(1);

  srand(time(NULL));
}
```





## soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.<br><br>
a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “`/home/[user]/shift2/drakor`”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
<br>
b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “`/drakor/romance`”, jenis drama korea action akan disimpan dalam “`/drakor/action`” , dan seterusnya.
<br>
c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “`/drakor/romance/start-up.png`”.
<br>
d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “`/drakor/romance/start-up.png`” dan “`/drakor/action/the-k2.png`”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)
<br>
e. Di setiap folder kategori drama korea buatlah sebuah file "`data.txt`" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.
<br>
```
kategori : romance

nama : start-up
rilis  : tahun 2020

nama : twenty-one-twenty-five
rilis  : tahun 2022

```

<b>Note dan Ketentuan Soal:</b><br>
<ul>
<li>File zip berada dalam drive modul shift ini bernama drakor.zip</li>
<li>File yang penting hanyalah berbentuk .png</li>
<li>Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore(_).</li>
<li>Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.</li>
<li>Gunakan bahasa pemrograman C (Tidak boleh yang lain).</li>
<li>Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).</li>
<li>[user] menyesuaikan nama user linux di os anda.</li>
</ul>

## Penyelesaian nomor 2: 
Pertama, kami melakukan inisiasi beberapa header, varibel dan fungsi :
```c
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
```
header yang kami gunakan adalah seperti terlihat diatas
```c
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```
variabel global yang diinisiasi adalah seperti diatas, variabel ini akan digunakan pada program untuk memudahkan kami dalam memproses sebuah perintah
```c
void processing(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0);
  }
}
```
Selanjutnya adalah fungsi `processing` yang memiliki 2 parameter, parameter pertama adalah variabel global key yang telah diinisiasi sebelumnya, parameter kedua adalah detail perintah yang akan dijalankan. Fungsi ini berguna untuk memproses sebuah perintah dengan fork dan execv.
### 2a. mengextract zip yang diberikan ke dalam folder “`/home/[user]/shift2/drakor`” dan menghapus folder-folder yang tidak dibutuhkan.
```c
void start()
{
  char *make_dir[] = {"mkdir", "-p", "/home/richard/shift2/drakor/", NULL};
  processing(keyMkdir, make_dir);
  char *unzip[] = {"unzip", "-qo", "/home/richard/drakor.zip", "-d", "/home/richard/shift2/drakor", NULL};
  processing(keyUnzip, unzip);
  char *remove[] = {"rm", "-r", "/home/richard/shift2/drakor/coding/", "/home/richard/shift2/drakor/song/", "/home/richard/shift2/drakor/trash", NULL};
  processing(keyRemove, remove);
}
```
fungsi `start()` diatas adalah fungsi yang digunkan untuk menyelesaikan no 2a. Di dalam fungsi tersebut terdapat 3 proses, yaitu membuat folder `home/richard/shift2/drakor` menggunakan variabel make_dir dan fungsi processing, selanjutnya melakukan unzip file yang berada di home dengan path `/home/richard/drakor.zip` dan hasilnya diletakkan di `/home/richard/shift2/drakor`, dan terakhir adalah menghapsu folder yang tidak diperlukan (coding, song, dan trash) menggunakan variabel remove dan fungsi processing <br>
<b>Output 2a :</b><br>
![Output result](img/unzip.png)
### 2b. membuat folder sesuai dengan category yang ada dalam file yang sudah diunzip
```c
void category_folder()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, token3);
          char *createFolder3[] = {"mkdir", "-p", path, NULL};
          processing("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path1[100] = "/home/richard/shift2/drakor/";
          char path2[100] = "/home/richard/shift2/drakor/";
          strcat(path1, token53);
          strcat(path2, token55);
          char *createFolder53[] = {"mkdir", "-p", path1, NULL};
          char *createFolder55[] = {"mkdir", "-p", path2, NULL};
          processing(keyMkdir, createFolder53);
          processing(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Untuk menyelesaikan fungsi ini, kami membuat sebuah fungsi yaitu `category_folder`.
```c
DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
   ...
  }
  else
  {
    exit(0);
  }
```
Pertama, kami membuka direktori `/home/richard/shift2/drakor/` dan mengecek apakah direktori tersebut memiliki isi atau tidak
```c
    while ((ep = readdir(dp)))
    {
        ...
    }
    (void)closedir(dp);
```
Setelah itu, kami membaca setiap isi dari folder tersebut dengan perulangan while, setelah semua file terbaca, direktori yang tadi dibuka akan ditutup
```c
    if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, token3);
          char *createFolder3[] = {"mkdir", "-p", path, NULL};
          processing("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path1[100] = "/home/richard/shift2/drakor/";
          char path2[100] = "/home/richard/shift2/drakor/";
          strcat(path1, token53);
          strcat(path2, token55);
          char *createFolder53[] = {"mkdir", "-p", path1, NULL};
          char *createFolder55[] = {"mkdir", "-p", path2, NULL};
          processing(keyMkdir, createFolder53);
          processing(keyMkdir, createFolder55);
        }
     }
```
program diatas berfungsi untuk membuat folder dengan membaca setiap category dari nama file yang ada dalam folder drakor dan membuat direktori baru sesuai hasilnya<br>
<b>Output 2b : </b><br>
![Output result](img/make_cat.png) 
### 2c dan 2d. memindahkan file sesuai kategori dan untuk file yang memiliki 2 kategori akan diduplikat dan dimasukkan ke 2 direktori. (revisi)
```c
void move_file()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define dest
          strcat(dest, token3);
          strcat(dest, "/");

          // define file sources
          strcat(file_path, fileName);

          char *moveFile3[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          char dest1[100];
          char dest2[100];
          char file_path1[100];
          char file_path2[100];

          // copy the path
          strcpy(dest1, path);
          strcpy(dest2, path);
          strcpy(file_path1, path);
          strcpy(file_path2, path);

          // define file sources
          strcat(file_path1, fileName);
          strcat(file_path2, fileName);

          // define dest1
          strcat(dest1, token53);
          strcat(dest1, "/");

          // define dest2
          strcat(dest2, token55);
          strcat(dest2, "/");

          char *copy_file[] = {"cp", file_path1, dest1, NULL};
          processing(keyCopy, copy_file);
          char *move_file[] = {"mv", file_path2, dest2, NULL};
          processing(keyMove, move_file);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Fungsi diatas adalah untuk memindahkan seluruh file yang ada sesuai category dan untuk file yang memiliki 2 kategori akan diduplikat dan dimasukkan ke dalam 2 folder sesuai kategori
<br><b>Output 2c dan 2d :</b><br>
![Output result](img/move_file.png) <br><br>
![Output result](img/move_file2.png)
### 2e. Pada setiap folder harus dibuat file data.txt yang isinya judul dan tahun dan disorting secara ascending menurut tahun. (revisi)
Berikut ini adalah fungsi yang diperlukan fungsi utama untuk melakukan 2e
```c
void sort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
  int i, key, j;
  char kuy[100];
  for (i = 1; i < n; i++)
  {
    key = arrTahun[i][0];
    strcpy(kuy, arrFilename[i]);
    j = i - 1;

    while (j >= 0 && arrTahun[j][0] > key)
    {
      arrTahun[j + 1][0] = arrTahun[j][0];
      strcpy(arrFilename[j + 1], arrFilename[j]);
      j = j - 1;
    }
    arrTahun[j + 1][0] = key;
    strcpy(arrFilename[j + 1], kuy);
  }
}
```
Fungsi tersebut akan mengurutkan tahun terbit
```c
void rename_txt(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "/home/richard/shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, whereFolder);
          strcat(path, "/");

          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define file sources
          strcat(file_path, fileName);

          // define dest
          strcat(dest, arr[0]);
          strcat(dest, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, whereFolder);
          strcat(path, "/");

          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define file sources
          strcat(file_path, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            // define dest
            strcat(dest, arr[0]);
            strcat(dest, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            // define dest
            strcat(dest, token522);
            strcat(dest, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile);
        }
      }
    }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    processing(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    sort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
Fungsi diatas berfungsi untuk merename nama file, membuat file data.txt dan melakukan pengurutan dengan fungsi sebelumnya dan menambahkan hasilnya ke file data.txt
<br><br> Fungsi dibawah ini adalah fungsi yang akan dipanggil untuk melakukan keseluruhan perintah
```c
void list()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        rename_txt(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```
<b>Output 2e : </b><br>
![Output result](img/isi_data.png) <br><br>
![Output result](img/isi_datac.png)
<br>Pada fungsi utama akan dipanggil fungsi yang diperlukan, seperti berikut ini
```c
int main()
{
  start();
  category_folder();
  move_file();
  list();
}
```
<b>Output keseluruhan : </b><br>
![Output result](img/folder_cat.png) <br><br>
![Output result](img/fol_action.png) <br><br>
![Output result](img/isi_data.png)
## Soal 3
Disediakan sebuah zip bernama `animal.zip`. Selanjutnya, dilakukan pengklasifikasian terhadap hewan-hewan dengan ketentuan: <br>
a. Program untuk membuat folder pada directory `/home/{user}/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat folder `air`.<br>
b. Program untuk melakukan extract zip `animal.zip` pada `/home/{user}/modul2/`.<br>
c. Hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama file. Hewan darat -> `/home/{user}/modul2/darat`, 3 detik kemudian, hewan air -> `/home/{user}/modul2/air`. Untuk file yang tidak ada keterangan hewan air atau darat, dihapus.<br>
d. Menghapus semua burung yang terdapat pada `/home/{user}/modul2/darat`, ditandai dengan adanya `bird`. <br>
e. Membuat file `list.txt` di folder `/home/{user}/modul2/air`. Masukkan list nama hewan dari folder air ke dalam `list.txt` dengan format `UID_[UID file permission]_Nama File.[jpg/png]`. UID = user dari file, permission = permission file. Contoh format nama: conan_rwx_hewan.png

> Notes untuk nomor 3:
> * tidak boleh memakai system()
> * tidak boleh memakai function C mkdir() ataupun rename()
> * gunakan exec dan fork
> * direktori "." dan ".." tidak termasuk

## Penyelesaian Nomor 3
Memasukkan library untuk digunakan dalam fungsi main.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>
```
## 3a
Pertama-tama, kami melakukan pendeklarasian terhadap variabel terlebih dahulu, yaitu:

```c
pid_t child_id1, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7, child_id8, child_id9;
int status;
```  
Selanjutnya membuat folder 'darat', pada pembuatan folder 'darat' ini kami menggunakan fork pada child dengan id 1 sehingga 'child_id1'. Lalu untuk membuat folder 'air', kami menggunakan fork pada 'child_id2' dengan program sebagai berikut: 
```c
//folder darat
    child_id1 = fork();
    if(child_id1 < 0){
    exit(EXIT_FAILURE);//Program akan berhenti, jika gagal membuat proses baru
    }

    if(child_id1 == 0){
        char *argv[] = {"mkdir", "-p", "/home/alda/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    //folder air
    child_id2 = fork();
    if(child_id2 < 0) exit(EXIT_FAILURE);

    if(child_id2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/alda/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
```
* Program akan berjalan ketika `child_id == 0`
* Untuk membuat folder, digunakan `char *argv` yang merupakan variabel untuk menyimpan command dan membuat folder baru, yaitu `"mkdir", "-p", "/home/alda/modul2/*nama folder*, NULL`.
* Lalu selanjutnya, command yang terletak pada `argv` akan dieksekusi dengan menggunakan `execv("/bin/mkdir", argv)`
* Selanjutnya untuk memberikan delay selama 3 detik setelah pembuatan folder `darat`, diberikan `sleep(3)`.

Berikut adalah screenshot output ketika program tersebut dijalankan:

Folder Darat:
![folder darat](/img/Folder%20darat.jpeg)

Folder Air (setelah 3 detik):
![folder air](/img/Folder%20air.jpeg)

## 3b
Untuk melakukan unzip dari file `animal.zip`, disini kami menggunakan fork pada child_id3 dengan program seperti dibawah ini:
```c
    //unzip animal.zip
    child_id3 = fork();

    if(child_id3 < 0) exit(EXIT_FAILURE);

    if(child_id3 == 0){
        //mengekstrak zip 
        char *argv[] = {"unzip", "-oq", "/home/alda/animal.zip", "-d", "/home/alda/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
```
* Selanjutnya command untuk unzip disimpan di variabel `argv` : `"unzip", "-oq", "/home/alda/animal.zip", "-d", "/home/alda/modul2/", NULL`.
    * `-o` digunakan untuk melakukan *overwrite* apabila folder sudah ada pada tempat tujuan
    * `q` digunakan supaya program tidak mengeluarkan prompt apapun setelah selesai melakukan `extract` zip.
    * `-d` digunakan untuk menunjukkan directory tempat menyimpan hasil *unzip*
* Command `argv` dieksekusi menggunakan `execv("usr/bin/unzip", argv)`

Berikut adalah screenshot output ketika program tersebut dijalankan:
![hasil unzip](/img/animal.jpeg)

## 3c (revisi)
Selanjutnya pada 3c ini kami diminta untuk memindahkan file sesuai dengan namanya, seperti air dan darat ke folder masing-masing, adapun program yang digunakan sebagai berikut:
```c
    //pindahin ke darat
    child_id4 = fork();
    if(child_id4 < 0) exit(EXIT_FAILURE);

    if(child_id4 == 0){
        execl("/usr/bin/find", "find", "/home/alda/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/alda/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    //pindahin ke air
    child_id5 = fork();
    if(child_id5 < 0) exit(EXIT_FAILURE);

    if(child_id5 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/alda/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/alda/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);
```
* Langkah pertama untuk memindahkan file, yaitu dilakukan `find` terlebih dahulu, terhadap seluruh file yang mengandung `*darat*` dan `*air*`. Setelah itu, dilakukan `"-exec", "mv", "-t", "/home/alda/modul2/darat"` dan `"-exec", "mv", "-t", "/home/alda/modul2/air/"` untuk memindahkan file-file tersebut ke dalam directory masing-masing.
* Diberikan `sleep(3)` pada pembuatan folder air untuk memberikan delay sebanyak 3 detik setelah pembuatan folder pertama yaitu folder darat.

Lalu selanjutnya, untuk menghapus file yang tersisa pada directory animal yang dimana tidak mengandung nama darat maupun air, berikut program yang digunakan:
```c
    //menghapus file : animal
    child_id6 = fork();

    if(child_id6 < 0) exit(EXIT_FAILURE);

    if(child_id6 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/alda/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
```
* `rm` merupakan perintah dalam linux yang dimana untuk melakukan *remove*
* `-f` atau *force* digunakan untuk memaksa program agar dapat langsung *remove* tanpa mengeluarkan prompt
* `/home/alda/modul2/animal/*` untuk menghapus seluruh file yang berada pada directory animal.

Berikut adalah screenshot output ketika program tersebut dijalankan:
Folder air:
![folder air](/img/3C%20air.jpeg)

Folder darat:
![folder darat](/img/3C%20darat.jpeg)

Folder animal:
![folder animal](/img/3C%20animal.jpeg)

## 3d (revisi)
Disini kami diminta untuk menghapus file yang memiliki nama **bird** pada folder darat, digunakan program sebagai berikut:
```c
    //menghapus bird
    child_id7 = fork();
    if(child_id7 < 0) exit(EXIT_FAILURE);

    if (child_id7 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/alda/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
```
* `rm` merupakan perintah dalam linux untuk melakukan *remove*
* `-f` atau *force* digunakan untuk memaksa program agar dapat langsung *remove* tanpa mengeluarkan prompt
* `/home/alda/modul2/darat/*bird*` untuk menghapus seluruh file yang mengandung tulisan **bird** pada folder darat.

Berikut adalah screenshot output dari soal 3d:
![folder darat tanpa burung](/img/darat2.jpeg)

## 3e (revisi)
Selanjutnya untuk memasukkan list nama file ke dalam file `list.txt` dengan format yang diberikan (UID_UID Permission_Filename), berikut program yang kami gunakan:
```c
//memasukkan format nama : uid_uid permission_filename
    child_id8 = fork();
    if(child_id8 < 0) exit(EXIT_FAILURE);
  
    if (child_id8 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/alda/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/alda/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
```
* `sh` digunakan untuk menjalankan command pada shell
* `ls -la /home/alda/modul2/air` ini untuk mendapatkan list detail seluruh permission dan nama user yang terdapat dalam `/home/alda/modul2/air`
* Untuk mengolah data pada `ls -la`, digunakan `awk` untuk melakukan print dan memasukkan ke dalamm `list.txt`.

Berikut adalah screenshot output dari soal 3e:

File list.txt pada folder air:

![list.txt di air](/img/3E%20air.jpeg)

Isi file list.txt:

![isi list.txt](/img/List.jpeg)
