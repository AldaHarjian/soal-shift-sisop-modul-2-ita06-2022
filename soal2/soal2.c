#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";

void processing(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0);
  }
}

void start()
{
  char *make_dir[] = {"mkdir", "-p", "/home/richard/shift2/drakor/", NULL};
  processing(keyMkdir, make_dir);
  char *unzip[] = {"unzip", "-qo", "/home/richard/drakor.zip", "-d", "/home/richard/shift2/drakor", NULL};
  processing(keyUnzip, unzip);
  char *remove[] = {"rm", "-r", "/home/richard/shift2/drakor/coding/", "/home/richard/shift2/drakor/song/", "/home/richard/shift2/drakor/trash", NULL};
  processing(keyRemove, remove);
}

void category_folder()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, token3);
          char *createFolder3[] = {"mkdir", "-p", path, NULL};
          processing("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path1[100] = "/home/richard/shift2/drakor/";
          char path2[100] = "/home/richard/shift2/drakor/";
          strcat(path1, token53);
          strcat(path2, token55);
          char *createFolder53[] = {"mkdir", "-p", path1, NULL};
          char *createFolder55[] = {"mkdir", "-p", path2, NULL};
          processing(keyMkdir, createFolder53);
          processing(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void move_file()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define dest
          strcat(dest, token3);
          strcat(dest, "/");

          // define file sources
          strcat(file_path, fileName);

          char *moveFile3[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char path[100] = "/home/richard/shift2/drakor/";
          char dest1[100];
          char dest2[100];
          char file_path1[100];
          char file_path2[100];

          // copy the path
          strcpy(dest1, path);
          strcpy(dest2, path);
          strcpy(file_path1, path);
          strcpy(file_path2, path);

          // define file sources
          strcat(file_path1, fileName);
          strcat(file_path2, fileName);

          // define dest1
          strcat(dest1, token53);
          strcat(dest1, "/");

          // define dest2
          strcat(dest2, token55);
          strcat(dest2, "/");

          char *copy_file[] = {"cp", file_path1, dest1, NULL};
          processing(keyCopy, copy_file);
          char *move_file[] = {"mv", file_path2, dest2, NULL};
          processing(keyMove, move_file);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void sort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
  int i, key, j;
  char kuy[100];
  for (i = 1; i < n; i++)
  {
    key = arrTahun[i][0];
    strcpy(kuy, arrFilename[i]);
    j = i - 1;

    while (j >= 0 && arrTahun[j][0] > key)
    {
      arrTahun[j + 1][0] = arrTahun[j][0];
      strcpy(arrFilename[j + 1], arrFilename[j]);
      j = j - 1;
    }
    arrTahun[j + 1][0] = key;
    strcpy(arrFilename[j + 1], kuy);
  }
}

void rename_txt(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "/home/richard/shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, whereFolder);
          strcat(path, "/");

          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define file sources
          strcat(file_path, fileName);

          // define dest
          strcat(dest, arr[0]);
          strcat(dest, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char path[100] = "/home/richard/shift2/drakor/";
          strcat(path, whereFolder);
          strcat(path, "/");

          char dest[100];
          char file_path[100];

          // copy the path
          strcpy(dest, path);
          strcpy(file_path, path);

          // define file sources
          strcat(file_path, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            // define dest
            strcat(dest, arr[0]);
            strcat(dest, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            // define dest
            strcat(dest, token522);
            strcat(dest, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", file_path, dest, NULL};
          processing(keyMove, moveFile);
        }
      }
    }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    processing(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    sort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void list()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/richard/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        rename_txt(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

int main()
{
  start();
  category_folder();
  move_file();
  list();
}
