#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <time.h>
#include <json-c/json.h>

int primonum = 79000; // define primogems amount

// prototype functions
void dbdownload(char *dbase, char *file);
void unzip(char *file);
void mkprocess(char *dir);
int rannum(int len);
void reduceprim(int *primo);
void gachaproc(int nth, char gen_file[100]);
void stop();
void zip();
void rmprocess(char *dir);

int main()
{
    pid_t pid, sid;

    pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/rasy/soal-shift-sisop-modul-2-ita01-2022/soal1")) < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    char *dbase[] = {
        "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};
    char *file[] = {"chara.zip", "weapon.zip"};
    char *dirgacha[] = {"gacha_gacha"};
    int gachacount = 0;

    dbdownload(dbase[0], file[0]);
    dbdownload(dbase[1], file[1]);
    unzip(file[0]);
    unzip(file[1]);

    mkprocess(dirgacha[0]);

    while (1)
    {
        int cekpity90 = gachacount - (gachacount % 90);
        char path[100], hitpity90[5];

        strcpy(path, dirgacha[0]);
        strcat(path, "/total_gacha_");
        sprintf(hitpity90, "%d", cekpity90);
        strcat(path, hitpity90);
        strcat(path, "/");

        mkprocess(path);

        for (int i = 0; i < 9; i++)
        {

            char gen_file[100], round[5];
            struct tm *ptr;
            time_t rawTime;
            rawTime = time(NULL);
            ptr = localtime(&rawTime);
            char temp[100];
            strftime(temp, 100, "%H:%M:%S", ptr);

            int checkRound = gachacount - (gachacount % 10);

            strcpy(gen_file, path);
            strcat(gen_file, "/");
            strcat(gen_file, temp);
            strcat(gen_file, "_gacha_");
            sprintf(round, "%d", checkRound);
            strcat(gen_file, round);
            strcat(gen_file, ".txt");

            for (size_t j = 0; j < 10; j++)
            {
                gachacount++;
                reduceprim(&primonum);
                gachaproc(gachacount, gen_file);
            }
            sleep(1);

            srand(time(NULL));
        }
    }
}

void dbdownload(char *dbase, char *file)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"wget", "--no-check-certificate", "-q", dbase, "-O", file, NULL};
        execv("/bin/wget", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}

void unzip(char *file)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"unzip", "-q", "-n", file, NULL};
        execv("/bin/unzip", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}

void mkprocess(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        DIR *checkDir = opendir(dir);
        if (checkDir)
        {
            closedir(checkDir); 
        }
        else
        {
            char *argv[] = {"mkdir", dir, NULL}; 
            execv("/bin/mkdir", argv);
        }
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}

int rannum(int len)
{
    return (rand() % len);
}

void zip()
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        chdir("/home/rasy/soal-shift-sisop-modul-2-ita01-2022/soal1/gacha_gacha/");
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", ".", "-i", "*", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}

void stop()
{
    exit(EXIT_SUCCESS);
}

void rmprocess(char *dir)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"rm", "-rf", dir, NULL};
        execv("/bin/rm", argv);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}

void reduceprim(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 100)
    {
        zip();
        rmprocess("weapons");
        rmprocess("characters");
        stop();
    }
}

void gachaproc(int nth, char gen_file[100])
{
    int len;
    char basepath[100], type[100];

    if (nth % 2 != 0)
    {
        len = 48;
        strcpy(basepath, "characters/");
        strcpy(type, "Characters");
    }
    if (nth % 2 == 0)
    {
        len = 130;
        strcpy(basepath, "weapons/");
        strcpy(type, "Weapons");
    }

    struct dirent *dp;
    DIR *dir = opendir(basepath);

    char temppath[50];
    int rn_gacha = rannum(len);
    if (dir != NULL)
    {
        for (int k = 0; k <= rn_gacha; k++)
        {
            dp = readdir(dir);
        }
        strcpy(temppath, dp->d_name);
        dp = NULL;
        closedir(dir);
    }
    else
    {
        perror("Couldn't open the directory");
    }

    strcat(basepath, temppath);
    FILE *fp;
    char temp[4096];
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    fp = fopen(basepath, "r");
    fread(temp, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(temp);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);
    json_object_object_get_ex(parsed_json, "name", &name);

    FILE *output;
    output = fopen(gen_file, "a");

    char n[5], rest_prim[10];
    sprintf(rest_prim, "%d", primonum);
    sprintf(n, "%d", nth);

    char fileContext[100];

    strcpy(fileContext, n);
    strcat(fileContext, "_");
    strcat(fileContext, type);
    strcat(fileContext, "_");
    strcat(fileContext, json_object_get_string(rarity));
    strcat(fileContext, "_");
    strcat(fileContext, json_object_get_string(name));
    strcat(fileContext, "_");
    strcat(fileContext, rest_prim);
    strcat(fileContext, "\n");
    fputs(fileContext, output);
    fclose(output);
}
