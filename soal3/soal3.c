#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(void){
    pid_t child_id1, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7, child_id8;
    int status;

    //folder darat
    child_id1 = fork();
    if(child_id1 < 0){
    exit(EXIT_FAILURE);//Program akan berhenti, jika gagal membuat proses baru
    }

    if(child_id1 == 0){
        char *argv[] = {"mkdir", "-p", "/home/alda/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    //folder air
    child_id2 = fork();
    if(child_id2 < 0) exit(EXIT_FAILURE);

    if(child_id2 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/alda/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
        
    //unzip animal.zip
    child_id3 = fork();

    if(child_id3 < 0) exit(EXIT_FAILURE);

    if(child_id3 == 0){
        //mengekstrak zip 
        char *argv[] = {"unzip", "-oq", "/home/alda/animal.zip", "-d", "/home/alda/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
    
    //pindahin ke darat
    child_id4 = fork();
    if(child_id4 < 0) exit(EXIT_FAILURE);

    if(child_id4 == 0){
        execl("/usr/bin/find", "find", "/home/alda/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/alda/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    //pindahin ke air
    child_id5 = fork();
    if(child_id5 < 0) exit(EXIT_FAILURE);

    if(child_id5 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/alda/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/alda/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);
        
    //menghapus file : animal
    child_id6 = fork();

    if(child_id6 < 0) exit(EXIT_FAILURE);

    if(child_id6 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/alda/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
        
     //menghapus bird
    child_id7 = fork();
    if(child_id7 < 0) exit(EXIT_FAILURE);

    if (child_id7 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/alda/modul2/darat/*bird*", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);

    //memasukkan format nama : uid_uid permission_filename
    child_id8 = fork();
    if(child_id8 < 0) exit(EXIT_FAILURE);
  
    if (child_id8 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/alda/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/alda/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
}
